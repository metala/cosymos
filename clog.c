#include <stdarg.h>
#include <stdio.h>

#include "clog.h"

clog_level_t cloglevel = CLOG_DEBUG;

const char * const clog_fatal_tag PROGMEM = "FATAL";
const char * const clog_error_tag PROGMEM = "ERROR";
const char * const clog_warn_tag PROGMEM =  "WARN";
const char * const clog_info_tag PROGMEM = "INFO";
const char * const clog_debug_tag PROGMEM = "DEBUG";

void clog_vfprintf(FILE *stream, clog_level_t level, const char *tag, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  if (level > cloglevel) {
    return;
  }

#ifdef __AVR__
  fprintf_P(stream, PSTR("[%s] "), tag);
  vfprintf_P(stream, fmt, args);
  fprintf_P(stream, PSTR("\n"));
#else
  fprintf(stream, "[%s] ", tag);
  vfprintf(stream, fmt, args);
  fprintf(stream, "\n");
#endif

  va_end(args);
}