#pragma once

#define USE_CLOG            1
#define USE_CMQ             1
#define USE_CTICK           1

#if USE_CMQ
#define CMQ_LEN             (1 << 3) /* 8 */
#define CMQ_LEN_MASK        (CMQ_LEN-1)
#endif

enum {
    PID_CONSUMER,
    PID_CONSUMER_ASYNC,
    PID_PRODUCER_SEQ,
    PID_PRODUCER_FIB,
    PID_PRODUCER_ASYNC_1,
    PID_PRODUCER_ASYNC_2,
    PID_COUNT
};

extern void cos_proc_call(void);
extern void cos_run(void);
