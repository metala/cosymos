CC=gcc
CFLAGS=-Wall -Werror --std=gnu99 -D_POSIX_C_SOURCE=200112L -g
LDFLAGS += -lpthread -lutil -lc

target = cosymos
src = ${wildcard *.c}
headers = ${wildcard *.h}

.PHONY: run all avr

all: bin/${target}

bin/${target}: Makefile $(src) $(headers)
	@mkdir -p bin/
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(src)

avr:
	@echo AVR-CC ${<}
	avr-gcc -Wall -gdwarf-2 -Os -std=gnu99 \
		-mmcu=atmega644p \
		-DF_CPU=8000000 \
		-fno-inline-small-functions \
		-ffunction-sections -fdata-sections \
		-Wl,--relax,--gc-sections \
		-Wl,--undefined=_mmcu,--section-start=.mmcu=0x910000 \
		-Wa,-adhln \
		$(src) -o avr/main.elf > avr/main.S
	@echo
	@avr-size avr/main.elf

run: bin/${target}
	bin/${target}

clean: 
	rm bin/${target}
