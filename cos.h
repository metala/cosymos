#pragma once

#include <stddef.h>
#include <stdint.h>

#include "ccmn.h"
#include "app_config.h"

#include "cproc.h"
#include "cmsg.h"

#if USE_CMQ
#include "cmq.h"
#endif

#if USE_CLOG
#include "clog.h"
#endif


typedef struct cproc {
  cpc_t pc;
  cpstate_t state;

  // Synchronous message-passing
  cprocid_t msgto;

#if USE_CMQ
  // Asynchronous message queue
  cmq_iter_t mq_inbox;
#endif
} cproc_t;

// === GLOBAL VARIABLES ===
extern cprocid_t cprocid;
extern cprocid_t csenderid;
extern cproc_t cprocs[PID_COUNT];
extern cerr_t cerr;

// === HELPER MACROS ===
#define cproc()                   (cprocs[cprocid])

// === EXPORTED FUNCTIONS ===
extern void cos_init(void);
extern cerr_t cos_activate(cprocid_t);
extern cerr_t cos_kill(cprocid_t);
extern void cos_dispatch(cprocid_t);

#if USE_CTICK
extern void cos_tick();
#endif
