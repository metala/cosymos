avr/ - directory that stores compiled binary and assembly for AVR  
main.c - test program entrypoint  
c*.{c,h} - CoSyMOS compoents  
app.{c,} - the applicaiton source  
app_config.{c,} - the OS configuraiton for the application  
Makefile - a make file  
README.md - readme file  
.gitignore - file that ignores certain files in the repository
