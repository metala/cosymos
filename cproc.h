#pragma once

#define CPROC_TERMINATED          0
#define CPROC_READY               1
#define CPROC_WAIT                2

#define cbegin()                                                            \
  switch (cproc().pc) {                                                         \
  case 0:

#define cstate(s) cproc().state = (s)

#define cmark(s)                                                            \
  do {                                                                         \
    cproc().pc = __LINE__;                                                      \
  case __LINE__:                                                               \
    cproc().state = (s);                                                        \
  } while (0)

#define cend()                                                              \
  cmark(CPROC_TERMINATED);                                                   \
  }

#define cexit(s)                                                            \
  do {                                                                         \
    cproc().pc = __LINE__;                                                      \
    cproc().state = (s);                                                        \
    return;                                                                    \
  case __LINE__:;                                                              \
  } while (0)

#define cwait(cond)                                                         \
  do {                                                                         \
    cproc().pc = __LINE__;                                                      \
  case __LINE__:                                                               \
    if (!(cond)) {                                                             \
      cproc().state = CPROC_WAITING;                                             \
      return;                                                                  \
    }                                                                          \
    cproc().state = CPROC_READY;                                                 \
  } while (0)

#define cyield() cexit(CPROC_READY)

#define cterminate()                                                        \
  do {                                                                         \
    cproc().pc = __LINE__;                                                      \
    cproc().state = CPROC_TERMINATED;                                            \
    return;                                                                    \
  case __LINE__:;                                                              \
  } while (0)
