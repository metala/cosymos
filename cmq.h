#pragma once

#include <stddef.h>
#include <stdint.h>

#include "ccmn.h"
#include "app_config.h"

#define CMQ_NULL  ((uint8_t) 0xFF)

typedef uint8_t cmq_iter_t;

typedef struct cmq_entry {
    cmq_iter_t next;
    cprocid_t from;
    cmsg_t msg;
} cmq_entry_t;

typedef struct cmq {
    cmq_entry_t buf[CMQ_LEN];
    cmq_iter_t slot;
    uint8_t capacity;
} cmq_t;

// === ERRORS ===
#define E_MQ_EMPTY   0x20
#define E_MQ_FULL    0x21

extern cmq_t cmq;

// === EXPORTED FUNCTIONS ===
void cmq_init(void);
extern cerr_t cmq_put(cprocid_t to, const cmsg_t *msg);
extern cerr_t cmq_get(cmq_entry_t *msg);
extern cerr_t cmq_get_from(cmq_entry_t *msg, cprocid_t from);
extern bool8_t cmq_full();

