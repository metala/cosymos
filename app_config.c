#include <stdint.h>

#include "cos.h"


// proc runnables
extern void consumer(void);
extern void consumer_async(void);
extern void producer_seq(void);
extern void producer_fib(void);
extern void producer_async_1(void);
extern void producer_async_2(void);

// proc dispatcher
void cos_proc_call() {
  // CDEBUG("CALL PID[%hhu] pc=%hu, state=0x%02hhx", cprocid, cproc().pc, cproc().state);
  switch (cprocid) {
  case PID_CONSUMER:
    consumer();
    break;
  case PID_CONSUMER_ASYNC:
    consumer_async();
    break;
  case PID_PRODUCER_SEQ:
    producer_seq();
    break;
  case PID_PRODUCER_FIB:
    producer_fib();
    break;
  case PID_PRODUCER_ASYNC_1:
    producer_async_1();
    break;
  case PID_PRODUCER_ASYNC_2:
    producer_async_2();
    break;
    default:
#if USE_CLOG
      CERROR("invalid process id: %hhu", cprocid);
#endif
  }
}

void cos_run() {
  cos_dispatch(PID_CONSUMER);
  cos_dispatch(PID_CONSUMER_ASYNC);
  cos_dispatch(PID_PRODUCER_SEQ);
  cos_dispatch(PID_PRODUCER_FIB);
  cos_dispatch(PID_PRODUCER_ASYNC_1);
  cos_dispatch(PID_PRODUCER_ASYNC_2);
}