#pragma once

#include <stdint.h>
#include <stdio.h>
#ifdef __AVR__
#include <avr/pgmspace.h>
#endif

// Log level enum
typedef uint8_t clog_level_t;
#define CLOG_NONE       0
#define CLOG_FATAL      1
#define CLOG_ERROR      2
#define CLOG_WARNING    3
#define CLOG_INFO       4
#define CLOG_DEBUG      5

#ifdef __AVR__
#define CLOG_STR(s) PSTR(s)
#else
#define CLOG_STR(s) (s)
#define PROGMEM
#endif

extern const char * const clog_fatal_tag PROGMEM;
extern const char * const clog_error_tag PROGMEM;
extern const char * const clog_warn_tag PROGMEM;
extern const char * const clog_info_tag PROGMEM;
extern const char * const clog_debug_tag PROGMEM;

// Functions
extern void clog_vfprintf(FILE *stream, clog_level_t lvl, const char *tag, const char *fmt, ...);

// Macros
#define CFATAL(fmt, ...) clog_vfprintf(stderr, CLOG_FATAL, clog_fatal_tag, CLOG_STR(fmt), ##__VA_ARGS__)
#define CERROR(fmt, ...) clog_vfprintf(stderr, CLOG_ERROR, clog_error_tag, CLOG_STR(fmt), ##__VA_ARGS__)
#define CWARN(fmt, ...) clog_vfprintf(stderr, CLOG_WARN, clog_warn_tag, CLOG_STR(fmt), ##__VA_ARGS__)
#define CINFO(fmt, ...) clog_vfprintf(stderr, CLOG_INFO, clog_info_tag, CLOG_STR(fmt), ##__VA_ARGS__)
#define CDEBUG(fmt, ...) clog_vfprintf(stderr, CLOG_DEBUG, clog_debug_tag, CLOG_STR(fmt), ##__VA_ARGS__)