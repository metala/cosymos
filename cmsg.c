#include "cmsg.h"

void cmsg_send_nonblock(cprocid_t to, cmsg_size_t size, void *ptr)
{
  cmsg = (cmsg_t){CMSG_TYPE_SEND, size, ptr};
}

void cmsg_skip()
{
  cmsg.type = CMSG_TYPE_SKIP;
}

void cmsg_reply(uint16_t status)
{
  cmsg.type = CMSG_TYPE_REPLY;
  cmsg.size = status;
  cmsg.ptr = NULL;
}

