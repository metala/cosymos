#include "app.h"
#include "app_config.h"
#include "cos.h"


void producer_seq() {
  // persisting variables
  static int counter;

  cbegin();
  counter = 0;

  for (;;) {
    do {
      cmsg_send(PID_CONSUMER, (uint16_t)counter++, NULL);
    } while(cmsg.type == CMSG_TYPE_SKIP);
  }
  cend();
}

void producer_fib() {
  static uint32_t a = 0;
  static uint32_t b = 1;

  cbegin();
  for (;;) {
    uint32_t t = a+b;
    a = b;
    b = t;
    if (b < a) {
      CERROR("fib: Overflow! Terminating...");
      cos_activate(PID_PRODUCER_ASYNC_2);
      cterminate();
    }

    do {
      cmsg_send(PID_CONSUMER, sizeof(b), &b);
    } while(cmsg.type == CMSG_TYPE_SKIP);
  }
  cend();
}

void producer_async_1() {
  // persisting variables
  static uint16_t counter;
  cmsg_t msg;

  cbegin();
  counter = 0xAA;

  for (;counter<0xBB;) {
    msg = (cmsg_t){.type=CMSG_TYPE_SEND, counter, NULL};
    cerr_t err = cmq_put(PID_CONSUMER_ASYNC, &msg);
    if (err == E_OK) {
      counter += 1;
    }
    msg = (cmsg_t){.type=CMSG_TYPE_SEND, counter, NULL};
    err = cmq_put(PID_CONSUMER_ASYNC, &msg);
    if (err == E_OK) {
      counter += 1;
    }
    cyield();
  }

  cend();
}
void producer_async_2() {
  // persisting variables
  static uint16_t counter;

  cbegin();
  counter = 0x1AA;

  for (;counter < 0x1CC;) {
    cmsg_t msg = {.type=CMSG_TYPE_SEND, counter, NULL};
    cerr_t err = cmq_put(PID_CONSUMER_ASYNC, &msg);
    if (err == E_OK) {
      counter += 1;
    }
    cyield();
  }

  cos_activate(PID_PRODUCER_ASYNC_1);
  cend();
}

void consumer() {
  cbegin();

  for (;;) {
    cmsg_recvfrom(PID_PRODUCER_SEQ);
    CINFO("seq: %u", cmsg.size);

    cmsg_recvfrom(PID_PRODUCER_FIB);
    if (cerr == E_OK) {
      CINFO("fibonacci: %llu", *((uint32_t *)cmsg.ptr));
    }

    cmsg_recvfrom(PID_PRODUCER_FIB);
    if (cerr == E_OK) {
      CINFO("fibonacci: %llu", *((uint32_t *)cmsg.ptr));
    }

    cmsg_recv();
    if (csenderid == PID_PRODUCER_SEQ) {
      CINFO("seq: %u", cmsg.size);
    } else if (csenderid == PID_PRODUCER_FIB) {
      CINFO("fibonacci: %llu", *((uint32_t *)cmsg.ptr));
    } else {
      cmsg_reply(E_NOT_OK);
    }
  }

  cend();
}

void consumer_async() {
  static uint8_t i;

  cbegin();

  for (;;) {
    for (i = 0; i < 2; i++) {
      cmq_entry_t e;
      if (cmq_get(&e) != E_OK) {
        cyield();
        continue;
      }
      CINFO("Async message from 0x%02hhx: type=0x%02hhx, size=%hu", e.from, e.msg.type, e.msg.size);
    }
    cyield();
  }

  cend();
}
