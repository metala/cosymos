#include <string.h>

#include "ccmn.h"
#include "cos.h"
#include "cproc.h"


cprocid_t cprocid;
cprocid_t csenderid;
cproc_t cprocs[PID_COUNT];
cmsg_t cmsg;
cerr_t cerr;

#if USE_CTICK
cticks_t cticks;
#endif

void cos_init() {
  memset(cprocs, 0x00, sizeof(cprocs));
#if USE_CMQ
  cmq_init();
#endif
  for (cprocid_t i=0; i<PID_COUNT; i += 1) {
    cprocs[i].state = CPROC_READY;
  }
}

void cos_dispatch(cprocid_t id)
{
  cerr = E_OK;
  cprocid = id;
  switch (cproc().state) {
  case CPROC_MSG_REQ:
    if (cprocs[cproc().msgto].state != CPROC_MSG_RECV) {
      break;
    }
    // call the sender to prepare the message
    cproc().state = CPROC_MSG_SEND;
    cos_proc_call();
    if (cproc().state != CPROC_MSG_WAIT_REPLY) {
#if USE_CLOG
      CERROR("PROC[%hhu] Expected state=0x04 (PROC_SENT), got state=0x%0hhx", cprocid, cproc().state);
#endif
    }

    // call the receiver
    csenderid = cprocid;
    cprocid = cproc().msgto;
    cos_proc_call();

    // implicit OK
    if (cmsg.type == CMSG_TYPE_SEND) {
      cmsg_reply(E_OK);
    }

    // Rerun the sender?
    cprocid = csenderid;
    // fallthrough
  case CPROC_MSG_WAIT_REPLY:
  case CPROC_WAIT:
    cproc().state = CPROC_READY;
    // fallthrough
  case CPROC_READY:
    cos_proc_call();
    break;
  case CPROC_MSG_RECV:
  case CPROC_TERMINATED:
    // do nothing
    break;
  default:
#if USE_CLOG
    CERROR("PROC[%hhu] Unknown state (0x%0hhx)", cprocid, cproc().state);
#endif
  }
}

cerr_t cos_activate(cprocid_t id)
{
  if (cprocs[id].state != CPROC_TERMINATED) {
    return E_NOT_OK;
  }

  cprocs[id].pc = 0;
  cprocs[id].state = CPROC_READY;

  return E_OK;
}

cerr_t cos_kill(cprocid_t id)
{
  if (cprocs[id].state == CPROC_TERMINATED) {
    return E_NOT_OK;
  }

  cprocs[id].pc = 0;
  cprocs[id].state = CPROC_TERMINATED;

  return E_OK;
}


#if USE_CTICK
void cos_tick()
{
  cticks += 1;
}
#endif