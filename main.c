#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "cos.h"


int main() {
  cos_init();
  fprintf(stderr, "=== CoSyMOS ===\n");

  cerr_t err = cos_activate(0);
  if (err != E_OK) {
    CERROR("Unable to activte 0");
  }

  for (unsigned int loop = 0;; loop += 1) {
    CINFO("=== Main Loop %u ===", loop);
#ifdef SIMAVR_SIMULATION
    uart0_get_status_reg();
#endif

    cos_run();

#ifndef __AVR__
    nanosleep((const struct timespec[]){{0, 200000000L}}, NULL);
#endif
  }

  return 0;
}