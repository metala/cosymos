#include "cos.h"

cmq_t cmq;

inline static bool8_t cmq_next_slot()
{
    if (cmq.capacity == 0){
        return FALSE;
    }

    cmq_iter_t start = cmq.slot;
    while (cmq.buf[cmq.slot].from != CMQ_NULL)
    {
        if (cmq.slot == start) {
            return FALSE;
        }
        cmq.slot = (cmq.slot + 1) & CMQ_LEN_MASK;
    }

    return TRUE;
}

void cmq_init() {
  for (cprocid_t i=0; i<PID_COUNT; i += 1) {
    cprocs[i].mq_inbox = CMQ_NULL;
  }
  for (cmq_iter_t it=0; it<CMQ_LEN; it += 1) {
    cmq.buf[it].from = CMQ_NULL; 
  }
  cmq.capacity = CMQ_LEN;
  cmq.slot = 0;
}

cerr_t cmq_put(cprocid_t to, const cmsg_t *msg)
{
    if (!cmq_next_slot()) {
        return E_MQ_FULL;
    }

    cmq_iter_t slot = cmq.slot;
    cmq.buf[slot].msg = *msg;
    cmq.buf[slot].from = cprocid;
    cmq.buf[slot].next = CMQ_NULL;

    cmq_iter_t it = cprocs[to].mq_inbox;
    if (it != CMQ_NULL) {
        while (cmq.buf[it].next != CMQ_NULL) {
            it = cmq.buf[it].next;
        }
        cmq.buf[it].next = slot;
    } else {
        cprocs[to].mq_inbox = slot;
    }

    cmq.slot = (slot + 1) & CMQ_LEN_MASK;
    cmq.capacity -= 1;
    return E_OK;
}

cerr_t cmq_get(cmq_entry_t *msg)
{
    cmq_iter_t it = cproc().mq_inbox;
    if (it == CMQ_NULL) {
        return E_MQ_EMPTY;
    }

    cmq_entry_t e = cmq.buf[it];
    cproc().mq_inbox = e.next;
    *msg = e;
    cmq.buf[it].from = CMQ_NULL;
    cmq.capacity += 1;

    return E_OK;
}

cerr_t cmq_get_from(cmq_entry_t *msg, cprocid_t from)
{
    cmq_iter_t it = cproc().mq_inbox;
    if (it == CMQ_NULL) {
        return E_MQ_EMPTY;
    }

    while (cmq.buf[it].from != from &&
           cmq.buf[it].from != CMQ_NULL) {
        it = cmq.buf[it].next;
    }
    if (cmq.buf[it].from == CMQ_NULL) {
        return E_MQ_EMPTY;
    }
    *msg = cmq.buf[it];

    return E_OK;
}

bool8_t cmsgq_full()
{
    return cmq.capacity == 0;
}