#pragma once

#include <stdint.h>

#define TRUE            1
#define FALSE           0

#define E_OK            0
#define E_NOT_OK        1
#define E_INVALID       2
#define E_TERMINATED    3

typedef uint8_t bool8_t;
typedef uint8_t cerr_t;
typedef uint8_t cprocid_t;
typedef uint8_t cpstate_t;
typedef uint16_t cpc_t;
typedef uint8_t cmsg_type_t;
typedef uint16_t cmsg_size_t;
typedef uint16_t cmsg_size_t;
typedef uint32_t cticks_t;

typedef struct cmsg {
  cmsg_type_t type;
  cmsg_size_t size;
  void *ptr;
} cmsg_t;
