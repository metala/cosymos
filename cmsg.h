#pragma once

#include <stddef.h>
#include "ccmn.h"

extern cmsg_t cmsg;

#define CMSG_TYPE_SEND            0x00
#define CMSG_TYPE_REPLY           0x01
#define CMSG_TYPE_SKIP            0x02

#define CPROC_MSG_REQ             0x10
#define CPROC_MSG_RECV            0x11
#define CPROC_MSG_SEND            0x12
#define CPROC_MSG_WAIT_REPLY      0x13

#define cmsg_send(to, size, ptr)                                               \
  cproc().msgto = to;                                                          \
  cexit(CPROC_MSG_REQ);                                                        \
  switch (cproc().state) {                                                     \
  case CPROC_READY:                                                            \
  case CPROC_MSG_WAIT_REPLY:                                                   \
    if (cmsg.type != CMSG_TYPE_SEND) {                                         \
      break;                                                                   \
    }                                                                          \
    return;                                                                    \
  case CPROC_MSG_SEND:                                                         \
    cmsg_send_nonblock(to, size, ptr);                                         \
    cstate(CPROC_MSG_WAIT_REPLY);                                              \
  default:                                                                     \
    return;                                                                    \
  }

#define cmsg_recv() cexit(CPROC_MSG_RECV)

#define cmsg_recvfrom(from)                                                    \
  do {                                                                         \
    cmsg_recv();                                                               \
    if (csenderid == (from)) {                                                 \
      break;                                                                   \
    } else if (cprocs[from].state == CPROC_TERMINATED) {                       \
      cerr = E_TERMINATED;                                                     \
      break;                                                                   \
    }                                                                          \
    cmsg_skip();                                                               \
  } while (1)

extern void cmsg_send_nonblock(cprocid_t to, cmsg_size_t size, void *ptr);
extern void cmsg_skip(void);
extern void cmsg_reply(uint16_t status);
